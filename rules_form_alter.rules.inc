<?php

/**
 * @file
 * Rules integration for hook_form_alter.
 */


// Define Update action options.
define('RULES_FORM_ALTER_UPDATE_ALLWAYS', 0);
define('RULES_FORM_ALTER_UPDATE_EXISTS', 1);
define('RULES_FORM_ALTER_UPDATE_NEW', 2);
// Define String match options.
define('RULES_FORM_ALTER_MATCH_CONTAINS', 0);
define('RULES_FORM_ALTER_MATCH_EXACT', 1);
define('RULES_FORM_ALTER_MATCH_START', 2);
define('RULES_FORM_ALTER_MATCH_END', 3);


/**
 * Implements hook_rules_event_info().
 */
function rules_form_alter_rules_event_info() {
  $events = array();
  $events['rules_form_alter'] = array(
    'label' => t('Form alter'),
    'group' => t('rules_form_alter'),
    'variables' => rules_form_alter_event_variables(),
    'access callback' => 'rules_form_alter_rules_access',
  );
  return $events;
}

/**
 * Provides the variables used by the rules_form_alter event.
 */
function rules_form_alter_event_variables() {
  $variables = array(
    'form_id' => array(
      'label' => t('Form ID'),
      'type' => 'text',
    ),
  );
  return $variables;
}

/**
 * Implements hook_rules_condition_info().
 */
function rules_form_alter_rules_condition_info() {
  $conditions = array();

  $conditions['rules_form_alter_form_id'] = array(
    'label' => t('Check form ID'),
    'parameter' => array(
      'form_id' => array(
        'type' => 'text',
        'label' => t('form_id'),
      ),
      'operation' => array(
        'type' => 'decimal',
        'label' => t('Operation'),
        'description' => t('Select which string operation you would like to use to match the form name.'),
        'options list' => 'rules_form_alter_string_match_options_list',
      ),
    ),
    'group' => t('rules_form_alter'),
    'callbacks' => array(
      'execute' => 'rules_form_alter_rules_condition_form_id',
    ),
  );
  return $conditions;
}


/**
 * Provides the options for the match operation used by rules actions.
 */
function rules_form_alter_string_match_options_list() {
  return array(
    RULES_FORM_ALTER_MATCH_EXACT => t('Exactly') ,
    RULES_FORM_ALTER_MATCH_CONTAINS => t('Contains'),
    RULES_FORM_ALTER_MATCH_START => t('Starts'),
    RULES_FORM_ALTER_MATCH_END => t('Ends'),
  );
}

/**
 * Implements hook_rules_action_info().
 */
function rules_form_alter_rules_action_info() {
  $actions = array();

  $actions['rules_form_alter_set_elemet_value'] = array(
    'label' => t('Set a form element single value'),
    'parameter' => array(
      'update_option' => array(
        'type' => 'decimal',
        'label' => t('Update Option'),
        'description' => t('Update only or always set.'),
        'options list' => 'rules_form_alter_set_update_options_list',
      ),
      'element' => array(
        'type' => 'text',
        'label' => t('element name'),
        'description' => t('The name of the element (use the token [form] if you want to set a form value).'),
      ),
      'key' => array(
        'type' => 'text',
        'label' => t('Key'),
      ),
      'value' => array(
        'type' => 'text',
        'label' => t('Value'),
      ),
    ),
    'group' => t('rules_form_alter'),
    'callbacks' => array(
      'execute' => 'rules_form_alter_action_set_element_value',
    ),
  );

  $actions['rules_form_alter_set_elemet'] = array(
    'label' => t('Set a form element'),
    'parameter' => array(
      'update_option' => array(
        'type' => 'decimal',
        'label' => t('Upadte Option'),
        'description' => t('Update only or allways set.'),
        'options list' => 'rules_form_alter_set_update_options_list',
      ),
      'element' => array(
        'type' => 'text',
        'label' => t('element name'),
        'description' => t('The name of the element.'),
      ),
      'element_array' => array(
        'type' => 'text',
        'label' => t('Array of key value for the element'),
        'description' => t('Make sure this contains only valid form array data and no undesirable PHP. Users must have "use PHP for settings" permission to set this. For an example use the output of rules action "Print a form element for the purpose of debugging."'),
      ),
    ),
    'group' => t('rules_form_alter'),
    'callbacks' => array(
      'execute' => 'rules_form_alter_action_set_element',
    ),
    'access callback' => 'rules_form_alter_php_integration_access',
  );

  $actions['rules_form_alter_debug_form'] = array(
    'label' => t('Debug form: Print a list of the form elements'),
    'parameter' => array(),
    'group' => t('rules_form_alter'),
    'callbacks' => array(
      'execute' => 'rules_form_alter_action_debug_form',
    ),
  );

  $actions['rules_form_alter_debug_elemet'] = array(
    'label' => t('Debug array element: Print a form element'),
    'parameter' => array(
      'element' => array(
        'type' => 'text',
        'label' => t('element name'),
        'description' => t('The name of the element (use the token [form] to print the entire form).'),
      ),
    ),
    'group' => t('rules_form_alter'),
    'callbacks' => array(
      'execute' => 'rules_form_alter_action_debug_elemet',
    ),
  );
  return $actions;
}

/**
 * PHP integration access callback.
 */
function rules_form_alter_php_integration_access() {
  return user_access('use PHP for settings');
}

/**
 * Provides the options for the Update Option used by rules actions.
 */
function rules_form_alter_set_update_options_list() {
  return array(
    RULES_FORM_ALTER_UPDATE_ALLWAYS => t('Always write (Add/Update)') ,
    RULES_FORM_ALTER_UPDATE_EXISTS => t('Update only if form element is found'),
    RULES_FORM_ALTER_UPDATE_NEW => t("Only set if element doesn't exist"),
  );
}

/**
 * Rules integration access callback.
 */
function rules_form_alter_rules_access($type, $name) {
  return user_access('Make rule based changes to forms');
}


/**
 * Rules condition: form_id.
 */
function rules_form_alter_rules_condition_form_id($form_id, $operation) {
  $rules_form_alter_data = drupal_static('rules_form_alter_data', array());
  if (empty($rules_form_alter_data)) {
    return FALSE;
  }

  switch ($operation) {
    case RULES_FORM_ALTER_MATCH_CONTAINS:
      return (strpos($rules_form_alter_data['id'], $form_id) !== FALSE);

    case RULES_FORM_ALTER_MATCH_EXACT:
      return ($rules_form_alter_data['id'] == $form_id);

    case RULES_FORM_ALTER_MATCH_START:
      return (strpos($rules_form_alter_data['id'], $form_id, 0) !== FALSE);

    case RULES_FORM_ALTER_MATCH_END:
      $needle_length = strlen($form_id);
      $haystack_length = strlen($rules_form_alter_data['id']);
      if ($needle_length > $haystack_length) {
        return FALSE;
      }
      return (substr_compare($rules_form_alter_data['id'], $form_id, -$needle_length) === 0);
  }
}


/**
 * Rules action: Set element value.
 *
 * Set a single element value using the specified $key. If the element is the
 * actual form then always update otherwise use the $update_option.
 */
function rules_form_alter_action_set_element_value($update_option, $element, $key, $value) {
  $rules_form_alter_data = drupal_static('rules_form_alter_data', array());

  // We must have a form set in the data variable.
  if (isset($rules_form_alter_data) &&  isset($rules_form_alter_data['form'])) {
    // If the element is the form itself then the update option is not relevant.
    if ($element == '[form]') {
      $rules_form_alter_data['form'][$key] = $value;
    }
    else {
      // It's ok to create a missing element as long as update option isn't
      // update existing.
      $create_missing = ($update_option != RULES_FORM_ALTER_UPDATE_EXISTS);
      // Try and get the element to be updated.
      $ref = &_rules_form_alter_find_element($rules_form_alter_data['form'], $element, $create_missing, $ok, $isnew);
      // If $element was found.
      if ($ok) {
        // If update option is always then update.
        if ($update_option == RULES_FORM_ALTER_UPDATE_ALLWAYS) {
          $ref[$key] = $value;
        }
        // If update option is existing only then it's ok to update as
        // $create_missing was set to FALSE and no new levels where created.
        if ($update_option == RULES_FORM_ALTER_UPDATE_EXISTS) {
          $ref[$key] = $value;
        }
        // If update option is update new and the element is new then update it.
        if (($update_option == RULES_FORM_ALTER_UPDATE_NEW) && $isnew) {
          $ref[$key] = $value;
        }
      }
    }
  }
}

/**
 * Rules action: set element.
 *
 * Set the entire form element array.
 */
function rules_form_alter_action_set_element($update_option, $element, $element_array) {
  $rules_form_alter_data = drupal_static('rules_form_alter_data', array());
  // We must have a form set in the data variable.
  if (isset($rules_form_alter_data) &&  isset($rules_form_alter_data['form'])) {
    // Get the array to set the element to.
    $value = eval('return ' . $element_array . ';');
    if ($value) {
      // It's ok to create a missing element as long as update option is't
      // update existing.
      $create_missing = ($update_option != 1);
      // Try and get the element to be updated.
      $ref = &_rules_form_alter_find_element($rules_form_alter_data['form'], $element, $create_missing, $ok, $isnew);
      // If $element was found.
      if ($ok) {

        // If update option is allways then update.
        if ($update_option == RULES_FORM_ALTER_UPDATE_ALLWAYS) {
          $ref = $value;
        }
        // If update option is existing only then its ok to update as
        // $create_missing was set to FALSE and no new levels where created.
        if ($update_option == RULES_FORM_ALTER_UPDATE_EXISTS) {
          $ref = $value;
        }
        // If update option is update new and the element is new then update it.
        if (($update_option == RULES_FORM_ALTER_UPDATE_NEW) && $isnew) {
          $ref = $value;
        }
      }
    }
    else {
      watchdog('rules_form_alter', 'Invalid value for the "Set a form element" action can\'t set $element to: $array.', array('$element' => $element, '$array' => $element_array));
    }
  }
}


/**
 * Rules action: debug from.
 *
 * A debuging function will list all elements on the form.
 */
function rules_form_alter_action_debug_form() {
  $rules_form_alter_data = drupal_static('rules_form_alter_data', array());
  if (isset($rules_form_alter_data) && isset($rules_form_alter_data['form'])) {
    $av = '';
    foreach ($rules_form_alter_data['form'] as $key => $value) {
      $av = $av . $key . '</br>';
    }
    drupal_set_message('the form elements are ' . $av);

  }
}

/**
 * Rules action: debug form element.
 *
 * A debuging function will print a form element.
 */
function rules_form_alter_action_debug_elemet($element) {
  $rules_form_alter_data = drupal_static('rules_form_alter_data', array());
  if (isset($rules_form_alter_data) && isset($rules_form_alter_data['form'])) {
    if ($element == '[form]') {
      $msg_options = array(
        '@element' => $element,
        '@value' => var_export($rules_form_alter_data['form'], TRUE),
      );
      $msg = t('The value of @element is @value', $msg_options);
      drupal_set_message($msg);
    }
    else {
      $ref = &_rules_form_alter_find_element($rules_form_alter_data['form'], $element, FALSE, $ok, $isnew);
      if ($ok) {
        $msg_options = array(
          '@element' => $element,
          '@value' => var_export($ref, TRUE),
        );
        $msg = t('The value of @element is @value', $msg_options);
        drupal_set_message($msg);
      }
    }

  }
}

/**
 * Util function that converts a path to an array reference.
 *
 * If create $create_missing is true will create all missing levels
 * as empty arrays
 *
 * @param array $array
 *   The array (form array) to search.
 * @param string $path
 *   The element path to search for, in the form of a/b/c.
 * @param bool $create_missing
 *   Create missing levels if path is not found.
 * @param bool $ok
 *   True if the element was found/created.
 *
 * Return string
 *   Array raference ['a']['b']['c'].
 */
function &_rules_form_alter_find_element(&$array, $path, $create_missing, &$ok, &$isnew) {
  $ref = &$array;
  $index_a = explode('/', $path);
  $ok = TRUE;
  $isnew = FALSE;
  foreach ($index_a as $index) {
    if (isset($ref[$index])) {
      $ref = &$ref[$index];
    }
    elseif ($create_missing) {
      $ref[$index] = array();
      $ref = &$ref[$index];
      $isnew = TRUE;
    }
    else {
      $ok = FALSE;
      break;
    }
  }
  return $ref;
}
