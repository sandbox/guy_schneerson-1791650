Rules form alter
==============================

OVERVIEW
========
This module allows developers and site builders to make form changes using
rules.

It allows developers to use their favorite hook_form_alter techniques and
provide less technical users access to this functionality.

The module can be used for the following:
  - Make conditional changes to forms.
  - learn about hook_form_alter() without needing to write a module.
  - Debug and study existing forms.
  - Works with commerce_stock: all actions are available from the add to cart
    validation event. Can be used to create additional form actions like
   'download product image'.


INSTALLATION
============

- Copy all contents of this package to your modules directory preserving
  subdirectory structure.

- Go to Administration > Modules to install this module.

- Go to Administration > People > Permissions to assign the
  “Make rule based changes to forms” permission to roles of your choice.

- Optionally to use the "Set a form element" action you will need to
  enable the "PHP filter" core module in Administration > Modules and assign the
  “use PHP for settings” permission in Administration > People > Permissions.

Form alter operations
=====================

"Form alter" event
  A rules event that is triggered for each form.
  Use this event to add you rule based form modifications.

"Check form ID" condition -
  A rules condition for checking the identity of a form. options exists for
  exact match, contains, starts and ends.
  Use this condition to make sure your form altering actions are triggered for
  the correct forms.

"Set a form element single value" action
  A rules action allowing you to set a single value of a form element.
  Each Drupal form is comprised of an array of elements each holding a
  configuration array holding key value pairs.
  This action takes the following arguments:
  - Update option: Options are always, update if found & update if missing.
  - Element name: The name of the form element. Example: author on comment form.
  - Key: the key to update. example: #access, #weight.
  - Value: value to set the key to can contain numbers, strings and 0 for FALSE,
    1 for TRUE.

"Set a form element" action
  A rules action allowing you to set an entire form element by providing an
  array of values.
  This action takes the following arguments:
  - Update option: Options are always, update if found & update if missing.
  - Element name: The name of the form element. Example: author on comment form.
  - Array of key value for the element: this must be in the format of:
      array ('key 1' => value, 'key x' => value,  )

"Debug form: Print a list of the form elements" action
   A rules action that will list all form element for he current form.
   Has no arguments.

"Debug array element: Print a form element" action
  A rules action that will print the element array.
  The output of this action can be modified and use for the "Set a form element"
  action.
  This action takes the following arguments:
  - Element name: The name of the element, can be any of the values printed by
    the "Debug form: Print a list of the form elements" action.

General notes
=================
- You can set multiple properties of a form elements using multiple actions like
  #markup, #weight & #default_value.

- You can create new form elements or modify existing once.

- You can reference nested elements for fieldsets in the format of:
 {fieldset}/{fieldset}/{form element}.
